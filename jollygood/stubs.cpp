#include "mednafen/mednafen.h"
#include "mednafen/mednafen-driver.h"
#include "mednafen/netplay-driver.h"
#include "mednafen/state-driver.h"
#include "mednafen/state_rewind.h"
#include "mednafen/movie-driver.h"
#include "mednafen/mempatcher-driver.h"
#include "mednafen/video-driver.h"
#include "mednafen/cdrom/CDAFReader.h"
#include "mednafen/net/Net.h"

namespace Mednafen {
    class CDAFReader_Stubby final : public CDAFReader {
        public:
        CDAFReader_Stubby(Stream *fp) { throw 0; }
        ~CDAFReader_Stubby() { }
        uint64_t Read_(int16_t *buffer, uint64_t frames) { return 0; }
        bool Seek_(uint64_t frame_offset) { return true; }
        uint64_t FrameCount(void) { return 0; }
    };

    CDAFReader* CDAFR_MPC_Open(Stream* fp) {
        return new CDAFReader_Stubby(fp);
    }

    CDAFReader* CDAFR_Vorbis_Open(Stream* fp) {
        return new CDAFReader_Stubby(fp);
    }

    void MDFND_OutputNotice(MDFN_NoticeType t, const char* s) noexcept {}

    void MDFND_OutputInfo(const char *s) noexcept {}

    void MDFND_MidSync(EmulateSpecStruct *espec, const unsigned flags) {}

    void MDFND_MediaSetNotification(uint32 drive_idx, uint32 state_idx, uint32 media_idx, uint32 orientation_idx) {}

    bool MDFND_CheckNeedExit(void) { return false; }

    void MDFND_NetplayText(const char* text, bool NetEcho) {}
    void MDFND_NetplaySetHints(bool active, bool behind, uint32 local_players_mask) {}

    void MDFND_SetStateStatus(StateStatusStruct *status) noexcept {}

    void MDFND_SetMovieStatus(Mednafen::StateStatusStruct*) noexcept {}

    void MDFNSRW_Begin(void) noexcept {}

    void MDFNSRW_End(void) noexcept {}

    bool MDFNSRW_Frame(bool) noexcept { return false; }
}

std::unique_ptr<Net::Connection> Net::Connect(const char* host, unsigned int port) { return nullptr; }

extern "C" {
int cputest_get_flags(void) { return 0; }
}
