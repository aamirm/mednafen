/*
Copyright (c) 2020, OpenEmu Team
Copyright (c) 2020, Rupert Carmichael
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
 
* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the following disclaimer
  in the documentation and/or other materials provided with the
  distribution.
* Neither the names of the copyright holders nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// TurboGrafx-16/PC Engine/CD 6-Button Pad supported games
static std::vector<std::string> pce_6b_games = {
    // PCE
    "0d9135be3267876bfec7f588abeda5bb", // Street Fighter II' - Champion Edition (Japan)
    "cff035e7d00159ad0879df71e9aa25ca", // Strip Fighter II (Japan) (Unl)
    
    // PCE CD identified by Redump cue sheet MD5 hash for now.
    "39b67de686bdea7f25cb17bb5dd59bf3", // Advanced V.G. (Japan)
    "a8123121b3f3a0fc4f15f68123e300de", // Flash Hiders (Japan)
    //"ee5429409475963c8e6d4dc1d3b8af47", // Garou Densetsu 2 - Aratanaru Tatakai (Japan) (FABT, FAET, FAFT) - cannot toggle in-game
    //"d78a5009974e8616665a7c89568c4619", // Ryuuko no Ken (Japan) (En,Ja,Es) (FABT) - cannot toggle 2/6 button mode in-game at "key assign" options.
    //"367a499bb93e61c36f2fdff4bea2fe5c", // Ryuuko no Ken (Japan) (En,Ja,Es) (FACT) - toggling 2/6 button mode in-game at "key assign" options breaks input, unlike "FABT" dump which does nothing.
    "e42f62d283c5e29bf32ab5c3d1d71f05", // Ys IV - The Dawn of Ys (Japan) (Rev 3)
    
    // Not compatible, may need Avenue Pad 3 instead of Avenue Pad 6 / Arcade Pad 6
    // Asuka 120% Maxima - Burning Fest. Maxima (Japan)
    // Far East of Eden - Tengai Makyou - Fuun Kabuki-den (Japan)
    // Forgotten Worlds (Japan)
    // Forgotten Worlds (USA) (En,Ja)
    // Garou Densetsu Special (Japan) (FABT)
    // World Heroes 2 (Japan) - needs the Avenue Pad 3
};
