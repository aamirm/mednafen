#ifndef MDFNJG_SERIAL_H
#define MDFNJG_SERIAL_H

class SerialReader {
public:
    void read(std::string& serial, const char *filename);
    virtual std::string readbin(std::string filename) { return ""; }
    
    bool check_vector(std::string &value,
        std::vector<std::string> &array) {
        return std::find(array.begin(), array.end(), value) != array.end();
    }
};

class SerialReaderSS : public SerialReader {
public:
    std::string readbin(std::string filename) override;
};

class SerialReaderPSX : public SerialReader {
public:
    std::string readbin(std::string filename) override;
    bool sbi_exists(const char *filename);
};

#endif
