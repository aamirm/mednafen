/*
Copyright (c) 2020, OpenEmu Team
Copyright (c) 2020, Rupert Carmichael
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
 
* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the following disclaimer
  in the documentation and/or other materials provided with the
  distribution.
* Neither the names of the copyright holders nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

static std::vector<std::string> ss_3dpad_games = { // 3D Control Pad Games
    "GS-9087",    // Advanced World War Sennen Teikoku no Koubou - Last of the Millennium (Japan)
    "GS-9076",    // Azel - Panzer Dragoon RPG (Japan) (Disc 1 - 4)
    "T-33901G",   // Baroque (Japan)
    "T-10627G",   // Battle Garegga (Japan)
    "T-7011H-50", // Black Fire (Europe)
    "T-7003G",    // Black Fire (Japan)
    "MK-81003",   // Black Fire (USA)
    "MK-81803",   // Burning Rangers (Europe) / (USA)
    "GS-9174",    // Burning Rangers (Japan)
    "T-19706G",   // Can Can Bunny Extra (Japan)
    "T-19703G",   // Can Can Bunny Premiere 2 (Japan) (Disc 1 - 2)
    "T-10314G",   // Choro Q Park (Japan)
    "T-23502G",   // Code R (Japan)
    "T-9507H",    // Contra - Legacy of War (USA)
    "610-6483",   // Christmas NiGHTS into Dreams... (Europe)
    "610-6431",   // Christmas NiGHTS into Dreams... (Japan)
    "MK-81067",   // Christmas NiGHTS into Dreams... (USA)
    "T-5029H-50", // Croc - Legend of the Gobbos (Europe) / (USA)
    "T-26410G",   // Croc! - Pau-Pau Island (Japan)
    //"T-9509H-50", // Crypt Killer (Europe)
    //"T-9518G",    // Henry Explorers (Japan)
    //"T-9509H",    // Crypt Killer (USA)
    "MK-81205",   // Cyber Speedway (Europe)
    "MK-81204",   // Cyber Speedway (USA)
    "GS-9022",    // Gran Chaser (Japan)
    "T-18510G",   // Daisuki (Japan) (Disc 1 - 2)
    "MK-81304",   // Dark Savior (Europe) / (USA)
    "T-22101G",   // Dark Savior (Japan)
    "MK-81213",   // Daytona USA Championship Circuit Edition (Europe) / (Korea) / (USA)
    "GS-9100",    // Daytona USA Circuit Edition (Japan)
    "MK-81218",   // Daytona USA C.C.E. Net Link Edition (USA)
    "MK-81804",   // Deep Fear (Europe) (Disc 1 - 2)
    "GS-9189",    // Deep Fear (Japan) (Disc 1 - 2)
    "T-15019G",   // Drift King Shutokou Battle '97 - Tsuchiya Keiichi & Bandou Masaaki (Japan)
    "MK-81071",   // Duke Nukem 3D (Europe) / (USA)
    "T-9111G",    // Dungeon Master Nexus (Japan)
    "MK-81076",   // Enemy Zero (Europe) / (USA) (Disc 1 - 3) (Game Disc)
    "T-30001G",   // Enemy Zero (Japan) (Disc 1 - 3) (Game Disc)
    "T-30004G",   // Enemy Zero (Japan) (Disc 1 - 3) (Game Disc) (Satakore)
    "MK-81084",   // Exhumed (Europe)
    "T-13205H",   // Powerslave (USA)
    "T-5710G",    // Fantastep (Japan)
    "MK-81073",   // Fighters Megamix (Europe) / (USA)
    "GS-9126",    // Fighters Megamix (Japan)
    "MK-81282",   // Formula Karts - Special Edition (Europe) (En,Fr,De,Es)
    "T-21701G",   // Fuusui Sensei - Feng-Shui Master (Japan)
    //"T-30603G",   // G Vector (Japan)
    "GS-9003",    // Gale Racer (Japan) (En,Ja)
    "GS-9086",    // Greatest Nine '96 (Japan)
    "T-5714G",    // GT24 (Japan)
    "MK-81202",   // Hang On GP '96 (Europe) / Hang-On GP (USA)
    "GS-9032",    // Hang On GP '95 (Japan)
    "T-12303H",   // Hardcore 4X4 (Europe)
    "T-13703H",   // TNN Motor Sports Hardcore 4X4 (USA)
    "T-4313G",    // Deka Yonku - Tough The Truck (Japan)
    //"MK-81802",   // House of the Dead, The (Europe) / (USA)
    //"GS-9173",    // House of the Dead, The (Japan)
    "T-25503G",   // Initial D - Koudou Saisoku Densetsu (Japan)
    "T-18008G",   // Jungle Park - Saturn Shima (Japan)
    "T-19723G",   // Kiss yori... (Japan)
    "MK-81065",   // Lost World - Jurassic Park, The (Europe) / (USA)
    "GS-9162",    // Lost World - Jurassic Park, The (Japan)
    "T-10611G",   // Magic Carpet (Japan)
    "MK-81210",   // Manx TT Super Bike (Europe) / (USA)
    "GS-9102",    // Manx TT Super Bike (Japan)
    "T-13004H",   // MechWarrior 2 - 31st Century Combat (Europe) / (USA)
    "T-23406G",   // MechWarrior 2 (Japan)
    "MK-81300",   // Mystaria - The Realms of Lore (Europe) / (USA)
    "MK-81303",   // Blazing Heroes (USA)
    "GS-9021",    // Riglordsaga (Japan)
    "MK-81020",   // NiGHTS into Dreams... (Europe) / (USA)
    "GS-9046",    // NiGHTS into Dreams... (Japan)
    "T-10613G",   // Nissan Presents - Over Drivin' GT-R (Japan)
    "T-9108G",    // Ochigee Designer Tsukutte Pon! (Japan)
    "T-9104G",    // Ooedo Renaissance (Japan)
    //"MK-81009",   // Panzer Dragoon (Europe) / (Korea) / (USA)
    //"GS-9015",    // Panzer Dragoon (Japan)
    "MK-81022",   // Panzer Dragoon Zwei (Europe) / (USA)
    "GS-9049",    // Panzer Dragoon Zwei (Japan)
    "MK-81307",   // Panzer Dragoon Saga (Europe) / (USA) (Disc 1 - 4)
    "T-19708G",   // Pia Carrot e Youkoso!! We've Been Waiting for You (Japan)
    "T-18711G",   // Planet Joker (Japan)
    "MK-081066",  // Quake (Europe) / (USA)
    "GS-9084",    // Riglordsaga 2 (Japan)
    "MK-81604",   // Sega Ages Volume 1 (Europe)
    "T-12707H",   // Sega Ages (USA)
    "GS-9109",    // Sega Ages - After Burner II (Japan)
    "GS-9197",    // Sega Ages - Galaxy Force II (Japan)
    "GS-9110",    // Sega Ages - OutRun (Japan)
    "GS-9181",    // Sega Ages - Power Drift (Japan)
    //"GS-9108",    // Sega Ages - Space Harrier (Japan)
    "GS-9116",    // Sega Rally Championship Plus (Japan)
    "MK-81215",   // Sega Rally Championship Plus Net Link Edition (USA)
    "MK-81216",   // Sega Touring Car Championship (Europe) / (USA)
    "GS-9164",    // Sega Touring Car Championship (Japan)
    "MK-81383",   // Shining Force III (Europe) / (USA)
    "GS-9175",    // Shining Force III - Scenario 1 - Outo no Kyoshin (Japan)
    "GS-9188",    // Shining Force III - Scenario 2 - Nerawareta Miko (Japan)
    "GS-9203",    // Shining Force III - Scenario 3 - Hyouheki no Jashinguu (Japan)
    "6106979",    // Shining Force III Premium Disc (Japan)
    "MK-81051",   // Sky Target (Europe) / (USA)
    "GS-9103",    // Sky Target (Japan)
    "MK-8106250", // Sonic 3D - Flickies' Island (Europe)
    "GS-9143",    // Sonic 3D - Flickies' Island (Japan)
    "MK-81062",   // Sonic 3D Blast (USA)
    "MK-81800",   // Sonic R (Europe) / (USA)
    "GS-9170",    // Sonic R (Japan)
    "MK-81079",   // Sonic Jam (Europe) / (USA)
    "GS-9147",    // Sonic Jam (Japan)
    "T-10616G",   // Soukyuu Gurentai (Japan)
    "T-10626G",   // Soukyuu Gurentai Otokuyou (Japan)
    "T-5013H",    // Soviet Strike (Europe) / (USA)
    "T-10621G",   // Soviet Strike (Japan)
    "T-1105G",    // Taito Chase H.Q. + S.C.I. (Japan)
    //"T-4801G",    // Tama - Adventurous Ball in Giddy Labyrinth (Japan)
    "T-14412G",   // Touge King the Spirits 2 (Japan)
    //"MK-81043",   // Virtua Cop 2 (Europe) / (Korea) / (USA)
    //"GS-9097",    // Virtua Cop 2 (Japan)
    "T-19718G",   // Virtuacall S (Japan) (Disc 1) (Game Honpen)
    "T-7104G",    // Virtual Kyoutei 2 (Japan)
    "MK-81024",   // Wing Arms (Europe) / (USA)
    "GS-9038",    // Wing Arms (Japan)
    "MK-81129",   // Winter Heat (Europe) / (USA)
    "GS-9177",    // Winter Heat (Japan)
    "T-11308H-50",// WipEout 2097 (Europe)
    "T-18619G",   // WipEout XL (Japan)
    "GS-9196",    // World Cup France '98 - Road to Win (Japan)
    "MK-81181",   // World League Soccer '98 (Europe)
    "MK-81113",   // World Series Baseball II (Europe) / (USA)
    "GS-9120",    // World Series Baseball II (Japan)
};

// Saturn Stunner / Virtua Gun supported games
static std::vector<std::string> ss_virtuagun_games = {
    "T-25408H",   // Area 51 (Europe)
    "T-18613G",   // Area 51 (Japan)
    "T-9705H",    // Area 51 (USA)
    "T-15102H",   // Chaos Control (Europe)
    "T-7006G",    // Chaos Control Remix (Japan)
    "T-9509H-50", // Crypt Killer (Europe)
    "T-9518G",    // Henry Explorers (Japan)
    "T-9509H",    // Crypt Killer (USA)
    "T-23202G",   // Death Crimson (Japan)
    //"T-16103H",   // Die Hard Trilogy (Europe) / (USA)
    //"GS-9123",    // Die Hard Trilogy (Japan)
    "MK-81802",   // House of the Dead, The (Europe) / (USA)
    "GS-9173",    // House of the Dead, The (Japan)
    "T-25417H",   // Maximum Force (Europe)
    "T-9707H",    // Maximum Force (USA)
    "GS-9088",    // Mechanical Violator Hakaider - Last Judgement (Japan)
    "MK-81087",   // Mighty Hits (Europe)
    "T-16604G",   // Mighty Hits (Japan)
    //"T-9510G",    // Policenauts (Japan)
    "T-30403H",   // Scud - The Disposable Assassin (USA)
    "MK-81015",   // Virtua Cop (Europe) / (USA)
    "GS-9060",    // Virtua Cop (Japan)
    "MK-81043",   // Virtua Cop 2 (Europe) / (Korea) / (USA)
    "GS-9097",    // Virtua Cop 2 (Japan)
};

static std::unordered_map<std::string, int> ss_md_games = { // Multi-disc Games
    { "T-21301G",   3 }, // 3x3 Eyes - Kyuusei Koushu S (Japan) (Disc 1 - 3)
    //{ "T-21301G",   3 }, // 3x3 Eyes - Kyuusei Koushu S (Japan) (Disc 3) (Special CD-ROM)
    { "MK-8109150", 2 }, // Atlantis - The Lost Tales (Europe) (En,De,Es) (Disc 1 - 2)
    { "MK-8109109", 2 }, // Atlantis - The Lost Tales (France) (Disc 1 - 2)
    { "T-21512G",   2 }, // Ayakashi Ninden Kunoichiban Plus (Japan) (Disc 1 - 2)
    { "GS-9076",    4 }, // Azel - Panzer Dragoon RPG (Japan) (Disc 1 - 4)
    { "T-19907G",   2 }, // BackGuiner - Yomigaeru Yuusha-tachi - Hishou-hen - Uragiri no Senjou (Japan) (Disc 1 - 2)
    { "T-19906G",   2 }, // BackGuiner - Yomigaeru Yuusha-tachi - Kakusei-hen - Guiner Tensei (Japan) (Disc 1 - 2)
    //{ "T-22402G",   2 }, // Bakuretsu Hunter (Japan) (Disc 1)
    //{ "T-22402G",   2 }, // Bakuretsu Hunter (Japan) (Disc 2) (Omake CD)
    //{ "T-19703G",   1 }, // Can Can Bunny Premiere 2 (Japan) (Disc 1)
    //{ "T-19703G",   1 }, // Can Can Bunny Premiere 2 (Japan) (Disc 2) (Can Bani Himekuri Calendar)
    { "GS-9172",    2 }, // Chisato Moritaka - Watarase Bashi & Lala Sunshine (Japan) (Disc 1 - 2)
    { "T-23403G",   2 }, // Chou Jikuu Yousai Macross - Ai Oboete Imasu ka (Japan) (Disc 1 - 2)
    //{ "T-7028H-18", 1 }, // Command & Conquer - Teil 1 - Der Tiberiumkonflikt (Germany) (Disc 1) (GDI)
    //{ "T-7028H-18", 1 }, // Command & Conquer - Teil 1 - Der Tiberiumkonflikt (Germany) (Disc 2) (NOD)
    //{ "T-7028H-50", 1 }, // Command & Conquer (Europe) (En,Fr,De) (Disc 1) (GDI)
    //{ "T-7028H-50", 1 }, // Command & Conquer (Europe) (En,Fr,De) (Disc 2) (NOD)
    //{ "T-7028H-09", 1 }, // Command & Conquer (France) (Disc 1) (GDI Disc)
    //{ "T-7028H-09", 1 }, // Command & Conquer (France) (Disc 2) (NOD Disc)
    //{ "GS-9131",    1 }, // Command & Conquer (Japan) (Disc 1) (GDI Disc)
    //{ "GS-9131",    1 }, // Command & Conquer (Japan) (Disc 2) (NOD Disc)
    //{ "T-7028H",    1 }, // Command & Conquer (USA) (Disc 1) (GDI Disc)
    //{ "T-7028H",    1 }, // Command & Conquer (USA) (Disc 2) (NOD Disc)
    { "T-16201H",   2 }, // Corpse Killer - Graveyard Edition (USA) (Disc 1 - 2)
    { "T-1303G",    2 }, // Creature Shock (Japan) (Disc 1 - 2)
    { "T-01304H",   2 }, // Creature Shock - Special Edition (USA) (Disc 1 - 2)
    { "T-36401G",   2 }, // Cross Tantei Monogatari - Motsureta Nanatsu no Labyrinth (Japan) (Disc 1 - 2)
    { "T-8106H-50", 2 }, // D (Europe) / (France) / (Germany) (Disc 1 - 2)
    { "T-8101G",    2 }, // D no Shokutaku (Japan) (Disc 1 - 2)
    { "T-8106H",    2 }, // D (USA) (Disc 1 - 2)
    { "T-18510G",   2 }, // Daisuki (Japan) (Disc 1 - 2)
    { "T-22701G",   3 }, // DeathMask (Japan) (Disc 1 - 3)
    { "MK-81804",   2 }, // Deep Fear (Europe) (Disc 1 - 2)
    { "GS-9189",    2 }, // Deep Fear (Japan) (Disc 1 - 2)
    { "T-15031G",   2 }, // Desire (Japan) (Disc 1 - 2)
    { "T-14420G",   2 }, // Devil Summoner - Soul Hackers (Japan) (Disc 1 - 2)
    { "T-16207H",   2 }, // Double Switch (USA) (Disc 1 - 2)
    { "T-20104G",   2 }, // Doukyuusei 2 (Japan) (Disc A - B)
    //{ "T-1245G",    1 }, // Dungeons & Dragons Collection (Japan) (Disc 1) (Tower of Doom)
    //{ "T-1245G",    1 }, // Dungeons & Dragons Collection (Japan) (Disc 2) (Shadow over Mystara)
    { "T-10309G",   2 }, // Eberouge (Japan) (Disc 1 - 2)
    //{ "T-16605G",   2 }, // Elf o Karu Monotachi (Japan) (Disc 1)
    //{ "T-16605G",   2 }, // Elf o Karu Monotachi (Japan) (Disc 2) (Omake Disc)
    //{ "T-16610G",   2 }, // Elf o Karu Monotachi II (Japan) (Disc 1)
    //{ "T-16610G",   2 }, // Elf o Karu Monotachi II (Japan) (Disc 2) (Omake Disc)
    //{ "MK-81076",   4 }, // Enemy Zero (Europe) (Disc 0) (Opening Disc)
    { "MK-81076",   3 }, // Enemy Zero (Europe) / (USA) (Disc 1 - 3) (Game Disc)
    //{ "T-30001G",   4 }, // Enemy Zero (Japan) (Disc 0) (Opening Disc)
    { "T-30001G",   3 }, // Enemy Zero (Japan) (Disc 1 - 3) (Game Disc)
    //{ "T-30004G",   4 }, // Enemy Zero (Japan) (Disc 0) (Opening Disc) (Satakore)
    { "T-30004G",   3 }, // Enemy Zero (Japan) (Disc 1 - 3) (Game Disc) (Satakore)
    { "T-15022G",   4 }, // Eve - Burst Error (Japan) (Disc 1 - 4) (Kojiroh Disc)
    //{ "T-15022G",   1 }, // Eve - Burst Error (Japan) (Disc 2) (Marina Disc)
    //{ "T-15022G",   1 }, // Eve - Burst Error (Japan) (Disc 3) (Terror Disc)
    //{ "T-15022G",   1 }, // Eve - Burst Error (Japan) (Disc 4) (Making Disc)
    { "T-15035G",   4 }, // Eve - The Lost One (Japan) (Disc 1 - 4) (Kyoko Disc)
    //{ "T-15035G",   1 }, // Eve - The Lost One (Japan) (Disc 2) (Snake Disc)
    //{ "T-15035G",   1 }, // Eve - The Lost One (Japan) (Disc 3) (Lost One Disc)
    //{ "T-15035G",   1 }, // Eve - The Lost One (Japan) (Disc 4) (Extra Disc)
    //{ "T-31503G",   1 }, // Falcom Classics (Japan) (Disc 1) (Game Disc)
    //{ "T-31503G",   1 }, // Falcom Classics (Japan) (Disc 2) (Special CD)
    { "T-34605G",   2 }, // Find Love 2 - Rhapsody (Japan) (Disc 1 - 2)
    { "T-20109G",   2 }, // Friends - Seishun no Kagayaki (Japan) (Disc 1 - 2)
    { "T-17005G",   2 }, // Game-Ware Vol. 4 (Japan) (Disc A - B)
    { "T-17006G",   2 }, // Game-Ware Vol. 5 (Japan) (Disc A - B)
    { "GS-9056",    2 }, // Gekka Mugentan Torico (Japan) (Disc A - B) (Kyouchou-hen)
    //{ "GS-9056",    1 }, // Gekka Mugentan Torico (Japan) (Disc B) (Houkai-hen)
    { "T-4507G",    2 }, // Grandia (Japan) (Disc 1 - 2)
    { "T-19904G",   3 }, // Harukaze Sentai V-Force (Japan) (Disc 1 - 3)
    { "T-21902G",   3 }, // Haunted Casino (Japan) (Disc A - C)
    //{ "T-21902G",   1 }, // Haunted Casino (Japan) (Disc B)
    //{ "T-21902G",   1 }, // Haunted Casino (Japan) (Disc C)
    { "T-19714G",   2 }, // Houkago Ren'ai Club - Koi no Etude (Japan) (Disc 1 - 2)
    { "T-2001G",    2 }, // Houma Hunter Lime Perfect Collection (Japan) (Disc 1 - 2)
    { "T-5705G",    2 }, // Idol Janshi Suchie-Pai II (Japan) (Disc 1 - 2)
    { "T-5716G",    3 }, // Idol Janshi Suchie-Pai Mecha Genteiban - Hatsubai 5 Shuunen Toku Package (Japan) (Disc 1 - 3)
    //{ "T-20701G",   1 }, // Interactive Movie Action - Thunder Storm & Road Blaster (Japan) (Disc 1) (Thunder Storm)
    //{ "T-20701G",   1 }, // Interactive Movie Action - Thunder Storm & Road Blaster (Japan) (Disc 2) (Road Blaster)
    { "T-5302G",    2 }, // J.B. Harold - Blue Chicago Blues (Japan) (Disc 1 - 2)
    //{ "T-34601G",   1 }, // Jantei Battle Cos-Player (Japan) (Disc 1)
    //{ "T-34601G",   1 }, // Jantei Battle Cos-Player (Japan) (Disc 2) (Making Disc)
    { "T-2103G",    2 }, // Jikuu Tantei DD (Dracula Detective) - Maboroshi no Lorelei (Japan) (Disc A - B)
    //{ "T-2103G",    1 }, // Jikuu Tantei DD (Dracula Detective) - Maboroshi no Lorelei (Japan) (Disc B)
    { "T-28002G",   3 }, // Kakyuusei (Japan) (Disc 1 - 3)
    { "GS-9195",    2 }, // Kidou Senkan Nadesico - The Blank of 3 Years (Japan) (Disc 1 - 2)
    { "T-14312G",   2 }, // Koden Koureijutsu - Hyaku Monogatari - Honto ni Atta Kowai Hanashi (Japan) (Disc 1 - 2) (Joukan)
    //{ "T-14312G",   1 }, // Koden Koureijutsu - Hyaku Monogatari - Honto ni Atta Kowai Hanashi (Japan) (Disc 2) (Gekan)
    { "T-14303G",   2 }, // Kuusou Kagaku Sekai Gulliver Boy (Japan) (Disc 1 - 2)
    //{ "GS-9152",    1 }, // Last Bronx (Japan) (Disc 1) (Arcade Disc)
    //{ "GS-9152",    1 }, // Last Bronx (Japan) (Disc 2) (Special Disc)
    { "T-26405G",   2 }, // Lifescape - Seimei 40 Okunen Haruka na Tabi (Japan) (Disc 1 - 2) (Aquasphere)
    //{ "T-26405G",   1 }, // Lifescape - Seimei 40 Okunen Haruka na Tabi (Japan) (Disc 2) (Landsphere)
    { "T-14403H",   2 }, // Lunacy (USA) (Disc 1 - 2)
    { "T-27906G",   2 }, // Lunar 2 - Eternal Blue (Japan) (Disc 1 - 2)
    { "T-18804G",   2 }, // Lupin the 3rd Chronicles (Japan) (Disc 1 - 2)
    //{ "T-25302G1",  1 }, // Mahjong Doukyuusei Special (Japan) (Disc 1) (Genteiban)
    //{ "T-25302G2",  1 }, // Mahjong Doukyuusei Special (Japan) (Disc 2) (Genteiban) (Portrait CD)
    //{ "T-25305G1",  1 }, // Mahjong Gakuensai (Japan) (Disc 1) (Genteiban)
    //{ "T-25305G2",  1 }, // Mahjong Gakuensai (Japan) (Disc 2) (Seiyuu Interview Hi CD) (Genteiban)
    //{ "T-2204G",    1 }, // Mahjong-kyou Jidai Cebu Island '96 (Japan) (Disc 1)
    //{ "T-2204G",    1 }, // Mahjong-kyou Jidai Cebu Island '96 (Japan) (Disc 2) (Omake Disk)
    { "T-36302G",   2 }, // Maria - Kimi-tachi ga Umareta Wake (Japan) (Disc 1 - 2)
    { "T-15038G",   3 }, // MeltyLancer Re-inforce (Japan) (Disc 1 - 3)
    { "T-14414G",   2 }, // Minakata Hakudou Toujou (Japan) (Disc 1 - 2)
    { "T-9109G",    3 }, // Moon Cradle (Japan) (Disc 1 - 3)
    { "MK-81016",   2 }, // Mr. Bones (Europe) / (USA) (Disc 1 - 2)
    { "GS-9127",    2 }, // Mr. Bones (Japan) (Disc 1 - 2)
    { "T-21303G",   2 }, // My Dream - On Air ga Matenakute (Japan) (Disc 1 - 2)
    //{ "T-35501G",   1 }, // Nanatsu Kaze no Shima Monogatari (Japan) (Disc 1)
    //{ "T-35501G",   1 }, // Nanatsu Kaze no Shima Monogatari (Japan) (Disc 2) (Premium CD)
    { "T-7616G",    3 }, // Nanatsu no Hikan (Japan) (Disc 1 - 3)
    { "GS-9194",    2 }, // Neon Genesis - Evangelion - Koutetsu no Girlfriend (Japan) (Disc 1 - 2)
    { "T-22205G",   3 }, // Noel 3 (Japan) (Disc 1 - 3)
    { "T-27803G",   2 }, // Ojousama Express (Japan) (Disc 1 - 2)
    //{ "T-21904G",   1 }, // Ousama Game (Japan) (Disc 1) (Ichigo Disc)
    //{ "T-21904G",   1 }, // Ousama Game (Japan) (Disc 2) (Momo Disc)
    { "MK-81307",   4 }, // Panzer Dragoon Saga (Europe) / (USA) (Disc 1 - 4)
    { "T-36001G",   8 }, // PhantasM (Japan) (Disc 1 - 8)
    //{ "T-20114G",   1 }, // Pia Carrot e Youkoso!! 2 (Japan) (Disc 1) (Game Disc)
    //{ "T-20114G",   1 }, // Pia Carrot e Youkoso!! 2 (Japan) (Disc 2) (Special Disc)
    { "T-9510G",    3 }, // Policenauts (Japan) (Disc 1 - 3)
    { "T-17402G",   2 }, // QuoVadis 2 - Wakusei Kyoushuu Ovan Rei (Japan) (Disc 1 - 2)
    { "GS-9011",    2 }, // Rampo (Japan) (Disc 1 - 2)
    { "T-30002G",   4 }, // Real Sound - Kaze no Regret (Japan) (Disc 1 - 4)
    { "T-5308G",    2 }, // Refrain Love - Anata ni Aitai (Japan) (Disc 1 - 2)
    { "MK-8180145", 4 }, // Riven - A Sequencia de Myst (Brazil) (Disc 1 - 4)
    { "MK-81801",   4 }, // Riven - The Sequel to Myst (Europe) (Disc 1 - 4)
    { "T-35503G",   4 }, // Riven - The Sequel to Myst (Japan) (Disc 1 - 4)
    { "T-14415G",   2 }, // Ronde (Japan) (Disc 1 - 2)
    { "T-19508G",   2 }, // Roommate W - Futari (Japan) (Disc 1 - 2)
    { "T-32602G",   2 }, // Sakura Taisen - Teigeki Graph (Japan) (Disc 1 - 2)
    { "GS-9037",    2 }, // Sakura Taisen (Japan) (Disc 1 - 2)
    { "GS-9169",    3 }, // Sakura Taisen 2 - Kimi, Shinitamou Koto Nakare (Japan) (Disc 1 - 3)
    { "GS-9160",    2 }, // Sakura Taisen Jouki Radio Show (Japan) (Disc 1 - 2)
    //{ "T-14410G",   1 }, // Sengoku Blade - Sengoku Ace Episode II (Japan) (Disc 1)
    //{ "T-14410G",   1 }, // Sengoku Blade - Sengoku Ace Episode II (Japan) (Disc 2) (Sengoku Kawaraban)
    //{ "T-30902G",   1 }, // Senkutsu Katsuryu Taisen - Chaos Seed (Japan) (Disc 1)
    //{ "T-30902G",   1 }, // Senkutsu Katsuryu Taisen - Chaos Seed (Japan) (Disc 2) (Omake CD)
    //{ "T-20106G",   1 }, // Sentimental Graffiti (Japan) (Disc 1) (Game Disc)
    //{ "T-20106G",   1 }, // Sentimental Graffiti (Japan) (Disc 2) (Second Window)
    { "T-14322G",   2 }, // Shiroki Majo - Mou Hitotsu no Eiyuu Densetsu (Japan) (Disc 1 - 2)
    { "GS-9182",    2 }, // Shoujo Kakumei Utena - Itsuka Kakumei Sareru Monogatari (Japan) (Disc 1 - 2)
    { "T-2205G",    2 }, // Soukuu no Tsubasa - Gotha World (Japan) (Disc 1 - 2)
    { "T-34001G",   2 }, // Sound Novel Machi (Japan) (Disc 1 - 2)
    { "T-21804G",   2 }, // Star Bowling, The (Japan) (Disc 1 - 2)
    { "T-21805G",   2 }, // Star Bowling Vol. 2, The (Japan) (Disc 1 - 2)
    //{ "T-7033H-50", 1 }, // Street Fighter Collection (Europe) (Disc 1)
    //{ "T-7033H-50", 1 }, // Street Fighter Collection (Europe) (Disc 2)
    //{ "T-1223G",    1 }, // Street Fighter Collection (Japan) (Disc 1)
    //{ "T-1223G",    1 }, // Street Fighter Collection (Japan) (Disc 2)
    //{ "T-1222H",    1 }, // Street Fighter Collection (USA) (Disc 1)
    //{ "T-1222H",    1 }, // Street Fighter Collection (USA) (Disc 2)
    { "T-1204G",    2 }, // Street Fighter II Movie (Japan) (Disc 1 - 2)
    { "T-5713G",    2 }, // Suchie-Pai Adventure - Doki Doki Nightmare (Japan) (Disc 1 - 2)
    { "T-1225G",    3 }, // Super Adventure Rockman (Japan) (Disc 1 - 3)
    { "T-21802G",   2 }, // Tenchi Muyou! Mimiri Onsen - Yukemuri no Tabi (Japan) (Disc 1 - 2)
    { "T-26103G",   2 }, // Tenchi Muyou! Toukou Muyou - Aniraji Collection (Japan) (Disc 1 - 2)
    { "T-14301G",   2 }, // Tengai Makyou - Daiyon no Mokushiroku - The Apocalypse IV (Japan) (Disc 1 - 2)
    //{ "T-20702G",   1 }, // Time Gal & Ninja Hayate (Time Gal) (Japan) (Disc 1)
    //{ "T-20702G",   1 }, // Time Gal & Ninja Hayate (Ninja Hayate) (Japan) (Disc 1)
    { "T-9529G",    2 }, // Tokimeki Memorial Drama Series Vol. 2 - Irodori no Love Song (Japan) (Disc 1 - 2)
    { "T-9532G",    2 }, // Tokimeki Memorial Drama Series Vol. 3 - Tabidachi no Uta (Japan) (Disc 1 - 2)
    { "T-1110G",    3 }, // Tokyo Shadow (Japan) (Disc 1 - 3)
    { "MK-81053",   2 }, // Torico (Europe) (Disc 1 - 2)
    { "T-35601G",   2 }, // Tutankhamen no Nazo - A.N.K.H (Japan) (Disc 1 - 2)
    //{ "T-37301G",   1 }, // Twinkle Star Sprites (Japan) (Disc 1)
    //{ "T-37301G",   1 }, // Twinkle Star Sprites (Japan) (Disc 2) (Omake CD)
    { "T-7017G",    3 }, // Unsolved, The (Japan) (Disc 1 - 3)
    //{ "T-19718G",   1 }, // Virtuacall S (Japan) (Disc 1) (Game Honpen)
    //{ "T-19718G",   1 }, // Virtuacall S (Japan) (Disc 2) (Shokai Gentei Yobikake-kun)
    { "T-14304G",   3 }, // Virus (Japan) (Disc 1 - 3)
    //{ "T-16706G",   1 }, // Voice Fantasia S - Ushinawareta Voice Power (Japan) (Disc 1)
    //{ "T-16706G",   1 }, // Voice Fantasia S - Ushinawareta Voice Power (Japan) (Disc 2) (Premium CD-ROM)
    { "T-1312G",    2 }, // Voice Idol Maniacs - Pool Bar Story (Japan) (Disc 1 - 2)
    { "GS-9007",    2 }, // WanChai Connection (Japan) (Disc 1 - 2)
    //{ "T-9103G",    1 }, // Wangan Dead Heat + Real Arrange (Japan) (Disc 1)
    //{ "T-9103G",    1 }, // Wangan Dead Heat + Real Arrange (Japan) (Disc 2) (Addition)
    { "T-20117G",   2 }, // With You - Mitsumeteitai (Japan) (Disc 1 - 2)
    { "T-37001G",   2 }, // Wizardry Nemesis - The Wizardry Adventure (Japan) (Disc 1 - 2)
    { "T-33005G",   4 }, // Zoku Hatsukoi Monogatari - Shuugaku Ryokou (Japan) (Disc 1 - 4)
};

static std::unordered_map<std::string, int> ss_mtap_games = { // Multitap Games
    { "T-6004G",     4 }, // America Oudan Ultra Quiz (Japan)
    { "T-20001G",    4 }, // Bakushou!! All Yoshimoto Quiz Ou Ketteisen DX (Japan)
    { "T-13003H50",  4 }, // Blast Chamber (Europe)
    { "T-13003H",    4 }, // Blast Chamber (USA)
    { "T-16408H",    4 }, // Break Point (Europe)
    { "T-9107G",     4 }, // Break Point (Japan)
    { "T-8145H",     4 }, // Break Point Tennis (USA)
    { "T-1235G",     3 }, // Capcom Generation - Dai-4-shuu Kokou no Eiyuu (Japan)
    { "T-8111H",     4 }, // College Slam (USA)
    { "T-14316G",    7 }, // Denpa Shounenteki Game (Japan)
    { "T-14318G",    7 }, // Denpa Shounenteki Game (Japan) (Reprint)
    { "MK-81071",    7 }, // Duke Nukem 3D (Europe) / (USA) (Death Tank Zwei mini-game has 7-player support)
    { "T-10302G",    4 }, // DX Jinsei Game (Japan)
    { "T-10310G",    4 }, // DX Jinsei Game II (Japan)
    { "T-10306G",    5 }, // DX Nippon Tokkyuu Ryokou Game (Japan)
    { "T-5025H-50",  8 }, // FIFA - Road to World Cup 98 (Europe) ** warning: broken multitap code **
    { "T-5025H",     8 }, // FIFA - Road to World Cup 98 (USA) ** warning: broken multitap code **
    { "T-5003H",     6 }, // FIFA Soccer 96 (Europe) / (USA)
    { "T-10606G",    6 }, // FIFA Soccer 96 (Japan)
    { "T-5017H",     8 }, // FIFA Soccer 97 (Europe) / (USA)
    { "T-4308G",     6 }, // Fire Prowrestling S - 6Men Scramble (Japan)
    { "T-14411G",    4 }, // Gouketsuji Ichizoku 3: Groove on Fight (Japan)
    { "MK-81035",    6 }, // Guardian Heroes (Europe) / (USA)
    { "GS-9031",     6 }, // Guardian Heroes (Japan)
    { "T-20902G",    4 }, // Hansha de Spark! (Japan)
    { "T-1102G",     4 }, // HatTrick Hero S (Japan)
    { "T-7015H",     4 }, // Hyper 3-D Pinball (USA) / Tilt! (Europe)
    { "T-7007G",     4 }, // Hyper 3D Pinball (Japan)
    { "T-3602G",     4 }, // J. League Go Go Goal! (Japan)
    { "T-9528G",     4 }, // J. League Jikkyou Honoo no Striker (Japan)
    { "GS-9034",     8 }, // J. League Pro Soccer Club o Tsukurou! (Japan)
    { "GS-9168",     8 }, // J. League Pro Soccer Club o Tsukurou! 2 (Japan)
    { "GS-9048",     4 }, // J. League Victory Goal '96 (Japan)
    { "GS-9140",     4 }, // J. League Victory Goal '97 (Japan)
    { "T-12003H50",  4 }, // Jonah Lomu Rugby (Europe)
    { "T-12003H09",  4 }, // Jonah Lomu Rugby (Europe)
    { "T-30306G",    4 }, // Keriotosse! (Japan)
    { "T-5010H",     8 }, // Madden NFL 97 (Europe) / (USA)
    { "T-5024H",     8 }, // Madden NFL 98 (Europe) / (USA)
    //{ "T-11401G",    4 }, // Masters - Harukanaru Augusta 3 (Japan)
    { "MK81103-50", 10 }, // NBA Action (Europe)
    { "MK-81103",   10 }, // NBA Action (USA)
    { "MK-81124",   10 }, // NBA Action 98 (Europe) / (USA)
    { "T-8120H-50",  4 }, // NBA Jam Extreme (Europe)
    { "T-8122G",     4 }, // NBA Jam Extreme (Japan)
    { "T-8120H",     4 }, // NBA Jam Extreme (USA)
    { "T-8102H-50",  4 }, // NBA Jam Tournament Edition (Europe)
    { "T-8102G",     4 }, // NBA Jam Tournament Edition (Japan)
    { "T-8102H",     4 }, // NBA Jam Tournament Edition (USA)
    { "T-5015H",    10 }, // NBA Live 97 (Europe) / (USA)
    { "T-5027H",     8 }, // NBA Live 98 (Europe) / (USA)
    { "MK-81111",    8 }, // NFL '97 (USA)
    { "T-8109H-50", 12 }, // NFL Quarterback Club 96 (Europe)
    { "T-8105G",    12 }, // NFL Quarterback Club 96 (Japan)
    { "T-8109H",    12 }, // NFL Quarterback Club 96 (USA)
    { "T-8136H-50", 12 }, // NFL Quarterback Club 97 (Europe)
    { "T-8116G",    12 }, // NFL Quarterback Club 97 (Japan)
    { "T-8136H",    12 }, // NFL Quarterback Club 97 (USA)
    { "T-5016H",     8 }, // NHL 97 (Europe) / (USA)
    { "T-10620G",    8 }, // NHL 97 (Japan)
    { "T-5026H-50", 12 }, // NHL 98 (Europe)
    { "T-5026H",    12 }, // NHL 98 (USA)
    { "MK-8100250", 12 }, // NHL All-Star Hockey (Europe)
    { "MK-81002",   12 }, // NHL All-Star Hockey (USA)
    { "MK-81122",    8 }, // NHL All-Star Hockey 98 (Europe) / (USA)
    { "T-7013H-50",  6 }, // NHL Powerplay (Europe)
    { "T-7012G",     6 }, // NHL Powerplay '96 (Japan)
    { "T-07013H",    6 }, // NHL Powerplay '96 (USA)
    { "T-5206G",     4 }, // Noon (Japan)
    { "T-07904H50",  4 }, // Olympic Soccer (Europe)
    { "T-07904H18",  4 }, // Olympic Soccer (Germany)
    { "T-7304G",     4 }, // Olympic Soccer (Japan)
    { "T-07904H",    4 }, // Olympic Soccer (USA)
    //{ "MK-81101",    4 }, // Pebble Beach Golf Links (Europe) / (USA)
    //{ "GS-9006",     4 }, // Pebble Beach Golf Links - Stadler ni Chousen (Japan)
    //{ "T-5011H",     4 }, // PGA Tour 97 (Europe) / (USA)
    //{ "T-10619G",    4 }, // PGA Tour 97 (Japan)
    { "MK-81084",    6 }, // Exhumed (Europe) (Death Tank mini-game has 6-player support)
    { "T-13205H",    6 }, // Powerslave (USA) (Death Tank mini-game has 6-player support)
    { "T-18001G",    6 }, // Seireki 1999 - Pharaoh no Fukkatsu (Japan) (Death Tank mini-game has 6-player support)
    { "MK-81070",   10 }, // Saturn Bomberman (Europe) / (USA)
    { "T-14302G",   10 }, // Saturn Bomberman (Japan)
    { "T-14321G",    4 }, // Saturn Bomberman Fight!! (Japan)
    { "GS-9043",     4 }, // Sega Ages - Rouka ni Ichidanto R (Japan)
    { "MK-81105",    4 }, // Sega International Victory Goal (Europe) / Worldwide Soccer - Sega International Victory Goal Edition (USA)
    { "GS-9044",     4 }, // Sega International Victory Goal (Japan)
    { "MK-81112",    4 }, // Sega Worldwide Soccer 97 (Europe) / (USA)
    { "MK-81123",    4 }, // Sega Worldwide Soccer 98 - Club Edition (Europe) / Worldwide Soccer 98 (USA)
    { "GS-9187",     4 }, // Sega Worldwide Soccer 98 (Japan)
    { "T-15902H50",  4 }, // Slam 'n Jam '96 featuring Magic & Kareem - Signature Edition (Europe)
    { "T-159056",    4 }, // Slam 'n Jam '96 featuring Magic & Kareem (Japan)
    { "T-159028H",   4 }, // Slam 'n Jam '96 featuring Magic & Kareem - Signature Edition (USA)
    { "T-8125H-50",  6 }, // Space Jam (Europe)
    { "T-8119G",     6 }, // Space Jam (Japan)
    { "T-8125H",     6 }, // Space Jam (USA)
    { "T-17702H",    8 }, // Street Racer (Europe)
    { "T-17702G",    8 }, // Street Racer Extra (Japan)
    { "T-8133H-50",  4 }, // Striker '96 (Europe)
    { "T-8114G",     4 }, // Striker '96 (Japan)
    { "T-8133H",     4 }, // Striker '96 (USA)
    { "T-5713G",     4 }, // Suchie-Pai Adventure - Doki Doki Nightmare (Japan) (Disc 1 - 2)
    { "MK-81033",    3 }, // Three Dirty Dwarves (Europe)
    { "GS-9137",     3 }, // Three Dirty Dwarves (Japan)
    { "T-30401H",    3 }, // Three Dirty Dwarves (USA)
    { "T-25411450",  4 }, // Trash It (Europe)
    { "MK-81180",    4 }, // UEFA Euro 96 England (Europe)
    { "T-31501G",    6 }, // Vatlva (Japan)
    { "GS-9002",     4 }, // Victory Goal (Japan)
    { "GS-9112",     4 }, // Victory Goal Worldwide Edition (Japan)
    { "T-8129H-50",  4 }, // Virtual Open Tennis (Europe)
    { "T-15007G",    4 }, // Virtual Open Tennis (Japan)
    { "T-8129H",     4 }, // Virtual Open Tennis (USA)
    { "MK-81129",    4 }, // Winter Heat (Europe) / (USA)
    { "GS-9177",     4 }, // Winter Heat (Japan)
    { "T-2002G",     4 }, // World Evolution Soccer (Japan)
    { "MK-81181",    4 }, // World League Soccer '98 (Europe)
    { "GS-9196",     4 }, // World Cup '98 France - Road to Win (Japan)
    { "T-8126H-50",  4 }, // WWF In Your House (Europe)
    { "T-8120G",     4 }, // WWF In Your House (Japan)
    { "T-8126H",     4 }, // WWF In Your House (USA)
};
